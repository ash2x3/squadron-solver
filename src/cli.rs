use structopt::StructOpt;
use crate::Score;

#[derive(Debug, StructOpt)]
pub struct Opt {
    /// the difficulty of the mission to solve
    #[structopt(long)]
    pub goal: Score<u16>,

    /// full list of squadron members, i.e. candidates for missions
    #[structopt(long = "member", short = "m")]
    pub squadron: Vec<Score<u16>>,

    /// squadron's base stats.
    /// for convenience, default is hardcoded to my current stats
    #[structopt(long, default_value = "80/100/100", rename_all = "kebab")]
    pub base_stats: Score<u16>,
}
