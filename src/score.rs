use std::{
    cmp::Ord,
    fmt::{self, Display, Formatter},
    ops::{Add, AddAssign},
    str::FromStr,
};

/// A triple of physical, mental, and tactical score values.
///
/// It's defined very generically because I am a nerd but in practice,
/// these are basically just going to be integers.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Score<T>(pub T, pub T, pub T);

impl<T> Display for Score<T>
where
    T: Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        // mimics the layout from the game
        write!(f, "{}/{}/{}", self.0, self.1, self.2)
    }
}

/// Error in parsing a [Score], either from one of the components being unparseable, or having the wrong amount.
#[derive(Debug, thiserror::Error)]
pub enum ParseScoreError<T, const N: usize>
where
    T: FromStr + fmt::Debug,
    <T as FromStr>::Err: fmt::Debug,
{
    #[error("failed to parse value")]
    FromStr(<T as FromStr>::Err),
    #[error("wrong number of values, needs {}", N)]
    NumValues,
}

// for structopt
impl<T> FromStr for Score<T>
where
    T: FromStr + fmt::Debug + Copy,
    <T as FromStr>::Err: fmt::Debug,
{
    type Err = ParseScoreError<T, 3>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s
            // split the input string on slashes
            .split('/')
            // parse each substring into a T
            .map(str::parse::<T>)
            // collect the results into a single Result
            .collect::<Result<Vec<T>, <T as FromStr>::Err>>()
            // turn the error into our own error type
            .map_err(ParseScoreError::FromStr)
            // make sure the length is correct
            .and_then(|vec| {
                if vec.len() == 3 {
                    // fearlessly index the vector
                    Ok(Score(vec[0], vec[1], vec[2]))
                } else {
                    // make our own error
                    Err(ParseScoreError::NumValues)
                }
            })
    }
}

/// Turn a 3-tuple into a [Score], for convenience
impl<T> From<(T, T, T)> for Score<T> {
    fn from((one, two, three): (T, T, T)) -> Self {
        Score(one, two, three)
    }
}

/// Vector sum of two [Score]s.
impl<T> Add for Score<T>
where
    T: Add,
{
    type Output = Score<<T as Add>::Output>;
    fn add(self, rhs: Self) -> Self::Output {
        Score(self.0 + rhs.0, self.1 + rhs.1, self.2 + rhs.2)
    }
}

impl<T> AddAssign for Score<T>
where
    T: AddAssign,
{
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0;
        self.1 += rhs.1;
        self.2 += rhs.2;
    }
}

impl<T> Score<T>
where
    T: Ord,
{
    /// Check whether this score is sufficient for a given goal score.
    /// This is a bit like an Ord comparison,
    /// but it is NOT a partial order!
    pub fn meets_goal(&self, goal: &Self) -> bool {
        self.0 >= goal.0 && self.1 >= goal.1 && self.2 >= goal.2
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    #[test]
    fn score_compare_to_goal() {
        let low = Score(1, 2, 3);
        let high = Score(300, 200, 100);
        assert!(!(low.meets_goal(&high)));
        assert!(high.meets_goal(&low));

        // this is why it's not a partial ord
        let llh = Score(1, 2, 300);
        let hhl = Score(100, 2, 3);
        assert!(!(llh.meets_goal(&hhl)));
        assert!(!(hhl.meets_goal(&llh)));
    }

    #[test]
    fn parse_score() {
        let input = "315/325/40";
        let result: Score<u16> = input.parse().unwrap();
        assert_eq!(result, Score(315, 325, 40));
    }

    #[test]
    fn parse_errors() {
        let res: Result<Score<u16>, ParseScoreError<u16, 3>> = "123/456/789/123".parse();
        res.expect_err("too many values");

        let res: Result<Score<u16>, ParseScoreError<u16, 3>> = "123/456".parse();
        res.expect_err("too few values");

        let res: Result<Score<u16>, ParseScoreError<u16, 3>> = "foo/bar/baz".parse();
        res.expect_err("parse int error");
    }

    proptest! {
        #[test]
        fn parse_score_no_crash(s in "\\PC*") {
            let _ = s.parse::<Score<u16>>();
        }

        #[test]
        fn parse_all_valid_scores(s in "[0-9]{4}/[0-9]{4}/[0-9]{4}") {
            s.parse::<Score<u16>>().unwrap();
        }

        #[test]
        fn parse_correctly(x in u16::MIN..=u16::MAX, y in u16::MIN..=u16::MAX, z in u16::MIN..=u16::MAX) {
            let to_parse = format!("{}/{}/{}", x, y, z);
            let Score(a, b, c) = to_parse.parse().unwrap();
            prop_assert_eq!((a, b, c), (x, y, z));
        }

        #[test]
        fn meets_goal_no_crash(x in u16::MIN..=u16::MAX, y in u16::MIN..=u16::MAX, z in u16::MIN..=u16::MAX, a in u16::MIN..=u16::MAX, b in u16::MIN..=u16::MAX, c in u16::MIN..=u16::MAX) {
            let (s1, s2) = (Score(x, y, z), Score(a, b, c));
            let _ = s1.meets_goal(&s2);
            let _ = s2.meets_goal(&s1);
        }

        #[test]
        fn meets_goal_correctness(x in 1..=50u16, y in 1..=50u16, z in 1..=50u16, a in 50..100u16, b in 50..100u16, c in 50..100u16) {
            prop_assert!(Score(a, b, c).meets_goal(&Score(x, y, z)));
            prop_assert!(Score(a, b, c).meets_goal(&Score(a, b, c)));
            prop_assert!(!(Score(x-1, y, z).meets_goal(&Score(x, y, z))));
        }
    }

    mod trait_methods {
        use super::*;
        proptest! {
            #[test]
            fn add_assign(x in 0..99u16, y in 0..99u16, z in 0..99u16, a in 0..99u16, b in 0..99u16, c in 0..99u16) {
                let mut s = Score(x, y, z);
                s += Score(a, b, c);
                let Score(ax, by, cz) = s;
                prop_assert_eq!(ax, a + x);
                prop_assert_eq!(by, b + y);
                prop_assert_eq!(cz, c + z);
            }

            #[test]
            fn from_tuple(x in u16::MIN..=u16::MAX, y in u16::MIN..=u16::MAX, z in u16::MIN..=u16::MAX) {
                let tuple = (x, y, z);
                let s: Score<u16> = tuple.into();
                prop_assert_eq!(s, Score(x, y, z));
            }

            #[test]
            fn display_agrees_with_parse(x in u16::MIN..=u16::MAX, y in u16::MIN..=u16::MAX, z in u16::MIN..=u16::MAX) {
                let score = Score(x, y, z);
                let displayed = score.to_string();
                let parsed = displayed.parse::<Score<u16>>();

                prop_assert!(parsed.is_ok());
                prop_assert_eq!(parsed.unwrap(), score);
            }
        }
    }
}
