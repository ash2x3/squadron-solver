use itertools::Itertools;
use structopt::StructOpt;
use std::fmt::Write;

mod score;
use score::Score;
mod cli;
use cli::Opt;

/// FFXIV squadron mission solver
///
/// This crate uses a brute-force search to find viable teams to send
/// on Squadron Missions in Final Fantasy XIV.
///
/// From a mathematical perspective, this problem is a version of the
/// [knapsack problem](https://en.wikipedia.org/wiki/List_of_knapsack_problems).
/// An algorithmic solution almost certainly exists, but the brute-force is
/// good enough for the author's purposes, and vastly simpler to understand.
///
/// At present, the bruteforcer only considers the raw score values,
/// and ignores e.g. affinity and chemistry bonuses, so it is likely to
/// miss out on some solutions.
fn main() {
    let opt = Opt::from_args();

    let Opt { goal, squadron, base_stats } = opt;

    // number of solutions found so far
    let mut num_solutions = 0;

    // buffer for writing our results to before we know how many total to say
    let mut out = String::new();

    println!("Goal:\t\t{}", goal);
    println!("Base stats: \t{}", base_stats);
    for team in squadron.iter().combinations(4) {
        let mut team_score = base_stats;
        debug_assert_eq!(team.len(), 4);
        for member in &team {
            team_score += **member;
        }

        if team_score.meets_goal(&goal) {
            num_solutions += 1;
            for (i, v) in team.iter().enumerate() {
                writeln!(out, "Squad {}:\t{}", i, v).unwrap();
            }
            writeln!(out, "Score:\t\t{}\n", team_score).unwrap();
        }
    }

    println!("{} solutions found\n", num_solutions);
    if num_solutions > 0 {
        println!("{}", out);
    }
}
